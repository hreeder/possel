#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64
import datetime
import functools
import logging
import os

import cryptography.exceptions
from cryptography.hazmat import backends
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf import pbkdf2
import peewee as p
from pircel import model
import tornado.web
from ldap3 import Server, Connection
from ldap3.core.exceptions import LDAPBindError


logger = logging.getLogger(__name__)


def cryptographically_strong_random_token():
    return base64.urlsafe_b64encode(os.urandom(20))


class UserModel(model.BaseModel):
    username = p.TextField(unique=True)


class TokenModel(model.BaseModel):
    user = p.ForeignKeyField(UserModel, related_name='tokens', on_delete='CASCADE')
    token = p.TextField(unique=True)
    expiry_date = p.DateTimeField()


def create_tables():
    model.database.create_tables([UserModel, TokenModel], safe=True)


def get_kdf(salt):
    kdf = pbkdf2.PBKDF2HMAC(algorithm=hashes.SHA224,
                            length=32,
                            salt=salt,
                            iterations=100000,
                            backend=backends.default_backend(),
                            )
    return kdf


def hash_password(salt, password):
    if not isinstance(salt, bytes):
        salt = salt.encode()
    if not isinstance(password, bytes):
        password = password.encode()

    kdf = get_kdf(salt)
    return base64.urlsafe_b64encode(kdf.derive(password)).decode()


def verify_password(salt, password, expected_hash):
    # This will be handled by LDAP
    pass


def set_password(user, password, save=True):
    # This will be handled by LDAP
    pass


def create_user(username, password):
    # This will be handled by LDAP
    pass


def check_password(username, password):
    logger.debug("Checking password from LDAP")
    server = Server('localhost')
    try:
        conn = Connection(server,
                          'uid=%s,ou=People,dc=tardis,dc=ed,dc=ac,dc=uk' % (username,),
                          password,
                          auto_bind=True)
        logger.debug("LDAP Bound")
    except LDAPBindError as e:
        logger.debug("LDAPBindError - Unable to bind: %s" % (e,))
        return None

    try:
        user = UserModel.get(username=username)
        logger.debug("Got existing user, returning it")
    except p.DoesNotExist:
        user = UserModel.create(username=username)
        logger.debug("Created new user, returning it")
    return user


def cleanup_tokens():
    TokenModel.delete().where(TokenModel.expiry_date < datetime.datetime.utcnow())


def delete_token(token):
    TokenModel.get(token=token).delete_instance()


def get_new_token(user):
    token = None
    while token is None:
        try:
            token = TokenModel.create(token=cryptographically_strong_random_token(),
                                      user=user,
                                      expiry_date=datetime.datetime.utcnow() + datetime.timedelta(days=30),
                                      )
        except p.IntegrityError:
            # Holy balls a collision happened, let the loop try again
            pass

    return token.token


def get_user_by_token(token_string):
    cleanup_tokens()
    try:
        token = TokenModel.get(token=token_string)
    except p.DoesNotExist:
        return None
    else:
        return token.user


def required(method):
    """ Works like tornado.web.authenticated except it just returns 401 on failure.

    As opposed to redirecting to a login URL; we're making an API, not a website.
    """
    @functools.wraps(method)
    def inner_method(self, *args, **kwargs):
        if not self.current_user:
            raise tornado.web.HTTPError(401)
        return method(self, *args, **kwargs)
    return inner_method


class LoginFailed(Exception):
    pass


def login_get_token(username, password, old_token):
    user = check_password(username, password)
    if user is not None:
        # successful login, get them a new token and revoke an existing old one
        if old_token is not None and get_user_by_token(old_token) == user:
            delete_token(old_token)
        new_token = get_new_token(user)
        return new_token
    raise LoginFailed()


def main():
    import argparse
    from playhouse import db_url
    parser = argparse.ArgumentParser(description='Create database tables')
    parser.add_argument('-d', '--database', help='Peewee database selector', default='sqlite:///possel.db')
    args = parser.parse_args()

    db = db_url.connect(args.database)
    model.database.initialize(db)
    model.initialize()
    create_tables()

if __name__ == '__main__':
    main()
